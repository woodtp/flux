import mplhep

from flux_uncertainties.visualization.style import (
    neutrino_labels,
    okabe_ito,
    xlabel_enu,
    ylabel_flux,
)


def plot_ppfx_universes(ax, fig, nu, universes, nominal, ppfx_correction) -> None:
    ax.axhline(0.6e-7, 3, 4, color="w", label="PPFX Input")

    mplhep.histplot(
        H=nominal,
        label=f"{neutrino_labels[nu]} nominal",
        color="k",
        lw=3,
        yerr=False,
        edges=False,
        binwnorm=1,
        ax=ax,
    )

    ax.axhline(0.6e-7, 3, 4, color="w", label="PPFX Output")

    mplhep.histplot(
        H=universes[0],
        label=f"{neutrino_labels[nu]} universes",
        color=okabe_ito["skyblue"],
        lw=1,
        yerr=False,
        edges=False,
        binwnorm=1,
        ax=ax,
    )
    mplhep.histplot(
        H=universes[1:],
        color=okabe_ito["skyblue"],
        lw=1,
        yerr=False,
        edges=False,
        binwnorm=1,
        ax=ax,
    )

    mplhep.histplot(
        H=ppfx_correction,
        label=f"{neutrino_labels[nu]} mean",
        color=okabe_ito["blue"],
        histtype="errorbar",
        xerr=True,
        capsize=4,
        elinewidth=4,
        marker=None,
        binwnorm=1,
        ax=ax,
    )

    mplhep.histplot(
        H=nominal,
        color="k",
        lw=3,
        yerr=False,
        edges=False,
        binwnorm=1,
        ax=ax,
    )

    ax.set_xlim(0, 6)
    ax.set_ylim(0, 1.4e-7)
    ax.set_xlabel(xlabel_enu)
    ax.set_ylabel(ylabel_flux)
    ax.set_xticks(ax.get_xticks(), fontweight="bold")

    handles, labels = ax.get_legend_handles_labels()

    leg = ax.legend(
        handles,
        labels,
        loc="best",
    )

    for item, label in zip(leg.legendHandles, leg.texts):
        if label._text in ["PPFX Input", "PPFX Output"]:
            width = item.get_window_extent(fig.canvas.get_renderer()).width
            label.set_ha("left")
            label.set_position((-2 * width, 0))

    fig.tight_layout()
