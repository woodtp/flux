import mplhep

from flux_uncertainties.visualization.style import (
    neutrino_labels,
    xlabel_enu,
    ylabel_flux,
)


def plot_ppfx_correction(ax, nominal_flux, flux_correction, nus, **kwargs) -> None:

    opts = {"xlim": (0, 6)}
    opts.update(kwargs)

    nominal_labels = (
        f"{neutrino_labels[nus[0]]} nominal",
        f"{neutrino_labels[nus[1]]} nominal",
    )

    correction_labels = (
        f"{neutrino_labels[nus[0]]} PPFX mean",
        f"{neutrino_labels[nus[1]]} PPFX mean",
    )

    mplhep.histplot(
        H=nominal_flux[0],
        edges=False,
        lw=2,
        color="k",
        label=nominal_labels[0],
        binwnorm=1,
        ax=ax,
    )

    mplhep.histplot(
        H=nominal_flux[1],
        edges=False,
        lw=2,
        color="gray",
        label=nominal_labels[1],
        binwnorm=1,
        ax=ax,
    )

    histplot_opts = {
        "binwnorm": 1,
        "histtype": "errorbar",
        "xerr": False,
        "elinewidth": 3,
        "capsize": 4,
        "marker": "o",
        "markersize": 14,
        "markeredgewidth": 2,
    }

    mplhep.histplot(
        H=flux_correction[0],
        label=correction_labels[0],
        color="C0",
        **histplot_opts,
    )

    mplhep.histplot(
        H=flux_correction[1],
        label=correction_labels[1],
        markerfacecolor="none",
        color="C1",
        **histplot_opts,
    )

    ax.set_xlim(opts["xlim"])
    if "ylim" in opts:
        ax.set_ylim(opts["ylim"])
    ax.set_ylabel(ylabel_flux)
    ax.set_xlabel(xlabel_enu)

    # removing errorbars from legend
    handles, labels = ax.get_legend_handles_labels()
    handles = [h[0] for h in handles]

    ax.legend(
        handles,
        labels,
        loc="upper center",
        numpoints=1,
        ncol=2,
        fontsize=22,
    )
