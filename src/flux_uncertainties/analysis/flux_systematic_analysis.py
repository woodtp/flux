import itertools
from typing import Callable, Optional

import numpy as np
import pandas as pd
from ROOT import TH1D, TH2D, TAxis  # type: ignore

from flux_uncertainties.analysis import uncertainty
from flux_uncertainties.analysis.beam_focusing_systematics import \
    BeamFocusingSystematics
from flux_uncertainties.analysis.hadron_production_systematics import \
    HadronProductionSystematics
from flux_uncertainties.analysis.helpers import (calculate_correlation_matrix,
                                                 convert_pandas_to_th1,
                                                 convert_pandas_to_th2)
from flux_uncertainties.analysis.principal_component_analysis import PCA


class FluxSystematicsAnalysis:
    __slots__ = (
        "nominal_flux_df",
        "ppfx_correction_df",
        "bin_edges",
        "th2_bins",
        "statistical_uncertainties",
        "statistical_uncertainties_fraction",
        "stat_uncert_matrix",
        "hadron_systematics",
        "beam_systematics",
        "pca_covariance_matrix",
        "pca_eigen_values",
        "pca_components",
        "_has_beam",
    )

    def __init__(
        self,
        nominal_flux_df: pd.DataFrame,
        ppfx_correction_df: pd.DataFrame,
        bin_edges: np.ndarray,
    ) -> None:
        self.nominal_flux_df = nominal_flux_df
        self.ppfx_correction_df = ppfx_correction_df
        self.bin_edges = bin_edges

    def run(self, pca_threshold: float = 1) -> None:
        self.statistical_uncertainties = uncertainty.extract_statistical_uncertainties(
            self.nominal_flux_df
        )

        self.statistical_uncertainties_fraction = (
            uncertainty.extract_statistical_uncertainties(
                self.nominal_flux_df, normalized=True
            )
        )

        self.stat_uncert_matrix = pd.DataFrame(
            np.diag(self.statistical_uncertainties) ** 2,
            index=self.statistical_uncertainties.index,
            columns=self.statistical_uncertainties.index,
        )

        self.hadron_systematics = HadronProductionSystematics(self.ppfx_correction_df)

        self.th2_bins = np.arange(
            self.hadron_systematics.correlation_matrices.loc["total"].shape[0] + 1
        )

        self._has_beam = len(self.nominal_flux_df["run_id"].unique()) > 1

        if self._has_beam:
            self.beam_systematics = BeamFocusingSystematics(
                beam_flux_df=self.nominal_flux_df,
                bin_edges=self.bin_edges,
                smoothing=True,
            )

        total_cov = self.hadron_systematics.covariance_matrices.loc[
            ("absolute", "total")
        ]

        pca = PCA(total_cov, threshold=pca_threshold)

        pca.fit()

        self.pca_covariance_matrix = pd.DataFrame(
            pca.new_covariance_matrix, index=total_cov.index, columns=total_cov.columns
        )

        self.pca_eigen_values = pca.eigenvalues_df
        self.pca_components = pca.principal_component_df

    @property
    def matrix_taxis(self) -> TAxis:
        matrix = self.total_covariance_matrix
        rows = matrix.index
        columns = matrix.columns

        xbins = np.arange(0, len(rows) + 1, dtype=float)
        nbinsx = xbins.shape[0] - 1

        taxis = TAxis(nbinsx, xbins)

        it = zip(enumerate(rows), enumerate(columns))

        for (ii, row), (jj, col) in it:
            labelx = f"{row[0]}-{row[1]}-{row[2]}"
            labely = f"{col[0]}-{col[1]}-{col[2]}"
            taxis.SetBinLabel(ii + 1, labelx)
            taxis.SetBinLabel(jj + 1, labely)

        return taxis

    @property
    def total_covariance_matrix(self) -> pd.DataFrame:
        hp_mat = self.pca_covariance_matrix
        total_mat = hp_mat + self.stat_uncert_matrix
        if self._has_beam:
            return (
                total_mat
                + self.beam_systematics.total_covariance_matrix.loc["absolute"]
            )
        return total_mat

    @property
    def total_correlation_matrix(self) -> pd.DataFrame:
        corr_mat = calculate_correlation_matrix(self.total_covariance_matrix)
        return corr_mat

    @property
    def flux_prediction(self) -> pd.DataFrame:
        total_sigma = pd.Series(
            np.sqrt(np.diag(self.total_covariance_matrix)),
            index=self.total_covariance_matrix.index,
            name="sigma",
        )

        ppfx_mean = self.hadron_systematics.ppfx_corrected_flux.loc["total", "mean"]

        return pd.concat([ppfx_mean, total_sigma], axis=1)

    def total_uncertainties_in_range(
        self, elow, ehigh, mat: Optional[pd.DataFrame] = None
    ) -> pd.DataFrame:
        if mat is None:
            mat = self.total_covariance_matrix

        bins = self.bin_edges
        bins_selected = np.where((bins >= elow) & (bins <= ehigh))[0]
        if bins_selected.shape == bins.shape:
            bin_slice = slice(None)
        else:
            bin_slice = slice(bins_selected[0], bins_selected[-1])

        df_list = []

        for horn in ["fhc", "rhc"]:
            index_slice = pd.IndexSlice[horn, :, bin_slice]

            cov_mat = (
                mat.loc[index_slice, index_slice]
                .droplevel(level="horn_polarity", axis=0)
                .droplevel(level="horn_polarity", axis=1)
            )

            nue_flux = self.flux_prediction.loc[(horn, "nue", bin_slice), "mean"].sum()

            nuebar_flux = self.flux_prediction.loc[
                (horn, "nuebar", bin_slice), "mean"
            ].sum()

            numu_flux = self.flux_prediction.loc[
                (horn, "numu", bin_slice), "mean"
            ].sum()

            numubar_flux = self.flux_prediction.loc[
                (horn, "numubar", bin_slice), "mean"
            ].sum()

            total_nue_flux = nue_flux + nuebar_flux
            total_numu_flux = numu_flux + numubar_flux

            ratio_uncert = uncertainty.ratio_uncertainty(
                cov=cov_mat,
                total_neutrino1_flux=total_nue_flux,
                total_neutrino2_flux=total_numu_flux,
            )

            nue_uncert = uncertainty.flux_uncertainty(
                cov=cov_mat.loc["nue", "nue"], total_flux=nue_flux
            )

            nuebar_uncert = uncertainty.flux_uncertainty(
                cov=cov_mat.loc["nuebar", "nuebar"], total_flux=nuebar_flux
            )

            nue_nuebar_uncert = uncertainty.flux_uncertainty(
                cov=cov_mat.loc[slice("nue", "nuebar"), slice("nue", "nuebar")],
                total_flux=total_nue_flux,
            )

            numu_uncert = uncertainty.flux_uncertainty(
                cov=cov_mat.loc["numu", "numu"], total_flux=numu_flux
            )

            numubar_uncert = uncertainty.flux_uncertainty(
                cov=cov_mat.loc["numubar", "numubar"], total_flux=numubar_flux
            )

            numu_numubar_uncert = uncertainty.flux_uncertainty(
                cov=cov_mat.loc[slice("numu", "numubar"), slice("numu", "numubar")],
                total_flux=total_numu_flux,
            )

            res_dict = {
                "nue": [nue_uncert],
                "nuebar": [nuebar_uncert],
                "nue+nuebar": [nue_nuebar_uncert],
                "numu": [numu_uncert],
                "numubar": [numubar_uncert],
                "numu+numubar": [numu_numubar_uncert],
                "nue+nuebar/numu+numubar": [ratio_uncert],
            }

            df = pd.DataFrame(res_dict)
            df_list.append(df)

        return pd.concat(df_list, keys=["fhc", "rhc"]).droplevel(level=-1)

    @property
    def total_flux_uncertainties_table(self) -> pd.DataFrame:
        bin_combos = itertools.combinations(self.bin_edges, 2)
        df_list = {}
        for combo in bin_combos:
            df_list[combo] = self.total_uncertainties_in_range(*combo)

        df1 = (
            pd.concat(df_list.values(), keys=df_list.keys())
            .swaplevel(i=2, j=1)
            .swaplevel(i=0, j=1)
            .sort_index()
        )

        if df1 is None:
            raise ValueError()

        return df1

    def export_matrices(
        self,
        matrix: pd.DataFrame,
        title_gen: str | Callable[[str], str],
    ) -> dict[str, TH2D]:
        if isinstance(title_gen, str):
            return {title_gen: convert_pandas_to_th2(matrix, hist_title=title_gen)}

        export_dict = {}

        for category, hist in matrix.groupby(level=0):
            hist_title = title_gen(str(category))
            export_dict[hist_title] = convert_pandas_to_th2(
                hist.droplevel(0), hist_title=hist_title
            )

        return export_dict

    @property
    def exportable_product_dict(
        self,
    ) -> dict[
        str,
        tuple[np.ndarray, np.ndarray] | tuple[np.ndarray, np.ndarray, np.ndarray],
    ]:
        product_dict = {}
        product_dict["matrix_TAxis"] = self.matrix_taxis
        stat_mat_title = "hmat_stat"
        stat_uncert_mat = convert_pandas_to_th2(
            self.stat_uncert_matrix, hist_title=stat_mat_title
        )
        product_dict[f"statistical_uncertainties/{stat_mat_title}"] = stat_uncert_mat

        for idx, hist in self.statistical_uncertainties_fraction.groupby(
            level=["horn_polarity", "neutrino_mode"]
        ):
            horn, nu = idx  # type: ignore
            hist_title = f"hstat_{horn}_{nu}"
            stat_uncert = convert_pandas_to_th1(
                series=hist, bin_edges=self.bin_edges, hist_title=hist_title
            )
            stat_uncert.SetTitle(";E_{#nu} (GeV);#sigma^{stat} / #Phi")
            product_dict[f"statistical_uncertainties/{hist_title}"] = stat_uncert

        for idx, hist in self.statistical_uncertainties.groupby(
            level=["horn_polarity", "neutrino_mode"]
        ):
            horn, nu = idx  # type: ignore
            hist_title = f"hstat_{horn}_{nu}_abs"
            stat_uncert = convert_pandas_to_th1(
                series=hist, bin_edges=self.bin_edges, hist_title=hist_title
            )
            stat_uncert.SetTitle(";E_{#nu} (GeV);#sigma^{stat} (m^{-2} POT^{-1})")
            product_dict[f"statistical_uncertainties/{hist_title}"] = stat_uncert

        for cat, series in self.hadron_systematics.fractional_uncertainties.items():
            for idx, hist in series.groupby(level=["horn_polarity", "neutrino_mode"]):
                horn, nu = idx  # type: ignore
                hist_title = f"hfrac_hadron_{cat}_{horn}_{nu}"
                frac_uncert = convert_pandas_to_th1(
                    series=hist,
                    bin_edges=self.bin_edges,
                    hist_title=hist_title,
                )
                frac_uncert.SetTitle(";E_{#nu} (GeV);Fractional Uncertainty")
                product_dict[
                    f"fractional_uncertainties/hadron/{cat}/{hist_title}"
                ] = frac_uncert

        for idx, series in self.hadron_systematics.ppfx_corrected_flux.groupby(
            level=("category", "horn_polarity", "neutrino_mode")
        ):
            hist_title = f"ppfx_corrected_flux/{idx[0]}/h{idx[0]}_{idx[1]}_{idx[2]}"  # type: ignore
            th1 = TH1D(hist_title, "", len(self.bin_edges) - 1, self.bin_edges)
            for b, flux in series.iterrows():
                th1.SetBinContent(b[-1], flux[0])  # type: ignore
                th1.SetBinError(b[-1], flux[1])  # type: ignore
            th1.SetTitle(";E_{#nu} (GeV); #Phi_{#nu} (m^{-2} POT^{-1})")
            product_dict[hist_title] = th1

        for idx, series in self.flux_prediction.groupby(
            level=("horn_polarity", "neutrino_mode")
        ):
            hist_title = f"flux_prediction/hflux_{idx[0]}_{idx[1]}"  # type: ignore
            th1 = TH1D(hist_title, "", len(self.bin_edges) - 1, self.bin_edges)
            for b, flux in series.iterrows():
                th1.SetBinContent(b[-1], flux[0])  # type: ignore
                th1.SetBinError(b[-1], flux[1])  # type: ignore
            th1.SetTitle(";E_{#nu} (GeV); #Phi_{#nu} (m^{-2} POT^{-1})")
            product_dict[hist_title] = th1

        matrix_objects = [
            (
                self.hadron_systematics.covariance_matrices.loc["fractional"],
                lambda x: f"covariance_matrices/hadron/{x}/hcov_{x}",
            ),
            (
                self.hadron_systematics.covariance_matrices.loc["absolute"],
                lambda x: f"covariance_matrices/hadron/{x}/hcov_{x}_abs",
            ),
            (
                self.hadron_systematics.correlation_matrices,
                lambda x: f"covariance_matrices/hadron/{x}/hcorr_{x}",
            ),
            (self.total_covariance_matrix, "hcov_total"),
            (self.total_correlation_matrix, "hcorr_total"),
        ]

        if self._has_beam:
            matrix_objects += [
                (
                    self.beam_systematics.covariance_matrices.loc["fractional"],
                    lambda x: f"covariance_matrices/beam/run_{x}/hcov_{x}",
                ),
                (
                    self.beam_systematics.covariance_matrices.loc["absolute"],
                    lambda x: f"covariance_matrices/beam/run_{x}/hcov_{x}_abs",
                ),
                (
                    self.beam_systematics.total_covariance_matrix.loc["fractional"],
                    "covariance_matrices/beam/hcov_total",
                ),
                (
                    self.beam_systematics.total_covariance_matrix.loc["absolute"],
                    "covariance_matrices/beam/hcov_total_abs",
                ),
                (
                    self.beam_systematics.total_correlation_matrix,
                    "covariance_matrices/beam/hcorr_total",
                ),
            ]

            for (
                run_id,
                series,
            ) in self.beam_systematics.fractional_uncertainties.items():
                group = series.groupby(level=["horn_polarity", "neutrino_mode"])
                for (horn, nu), hist in group:  # type: ignore
                    hist_title = f"hfrac_beam_{run_id}_{horn}_{nu}"
                    th1 = convert_pandas_to_th1(
                        series=hist,
                        bin_edges=self.bin_edges,
                        hist_title=hist_title,
                    )
                    th1.SetTitle(";E_{#nu} (GeV); Fractional Uncertainty")
                    product_dict[
                        f"fractional_uncertainties/beam/{run_id}/{hist_title}"
                    ] = th1

            # beam_frac_total_cov_groups = (
            #     self.beam_systematics.total_covariance_matrix.loc["fractional"].groupby(
            #         level=["horn_polarity", "neutrino_mode"]
            #     )
            # )
            # for (horn, nu), group in beam_frac_total_cov_groups:
            #     total_uncert = np.sqrt(np.diag(group.loc[(horn, nu), (horn, nu)]))
            #     hist_title = f"fractional_uncertainties/beam/htotal_{horn}_{nu}"
            #
            #     th1 = TH1D(hist_title, "", len(self.bin_edges) - 1, self.bin_edges)
            #
            #     for b, val in enumerate(total_uncert):
            #         th1.SetBinContent(b + 1, val)
            #
            #     th1.SetTitle(";E_{#nu} (GeV); Fractional Uncertainty")
            #
            #     product_dict[hist_title] = th1

        for mat, title_gen in matrix_objects:
            product_dict |= self.export_matrices(mat, title_gen)

        # product_dict["pca/hpca_cov"] = convert_pandas_to_th2(
        #     dataframe=self.pca_covariance_matrix.loc["fractional"],
        #     hist_title="hpca_cov",
        # )

        product_dict["pca/hcov_pca"] = convert_pandas_to_th2(
            dataframe=self.pca_covariance_matrix,
            hist_title="hcov_pca",
        )

        for pc, hist in self.pca_components.iterrows():
            hist_title = "_".join(map(str, pc))  # type: ignore
            bin_edges = np.arange(1, len(hist.index) + 1, dtype=float)
            th1 = convert_pandas_to_th1(
                series=hist, bin_edges=bin_edges, hist_title=f"hpc_{hist_title}"
            )
            if "scaled" in hist_title:
                th1.SetTitle(";E_{#nu} Bin Number; #lambda_{n} #hat{e}_{n}")
            else:
                th1.SetTitle(";E_{#nu} Bin Number; #hat{e}_{n}")
            product_dict[f"pca/components/hpc_{hist_title}"] = th1

        return product_dict
