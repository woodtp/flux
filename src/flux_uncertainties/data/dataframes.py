from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import multiprocessing as mp
from functools import partial
from typing import Optional

import pandas as pd
from pandas import DataFrame
from rich.progress import track

import uproot


TH1D = uproot.models.TH.Model_TH1D_v3  # type: ignore


def construct_dataframe(
    hist: TH1D,
    horn_polarity: str,
    neutrino_mode: str,
    category: Optional[str] = None,
    universe: Optional[int] = None,
    run_id: Optional[int] = None,
) -> pd.DataFrame:
    bin_edges = hist.axis().edges()

    bin_edges_low = [bin_edges[idx] for idx in range(len(bin_edges) - 1)]
    bin_edges_up = [bin_edges[idx + 1] for idx in range(len(bin_edges) - 1)]

    n_vals = int(hist.values().shape[0])

    df_dict = {
        "horn_polarity": n_vals * [horn_polarity],
        "neutrino_mode": n_vals * [neutrino_mode],
        "flux": hist.values(),
        "stat_uncert": hist.errors(),
        "bin": range(1, n_vals + 1),
        "E_low": bin_edges_low,
        "E_high": bin_edges_up,
    }

    if category is not None:
        df_dict["category"] = n_vals * [category]

    if universe is not None:
        df_dict["universe"] = n_vals * [universe]

    if run_id is not None:
        df_dict["run_id"] = n_vals * [run_id]

    return pd.DataFrame(df_dict)


def construct_nominal_dataframe(
    rootfile: str, horn_polarity: str, run_id: Optional[int] = None
) -> DataFrame:
    df_list: list[pd.DataFrame] = []

    with uproot.open(rootfile) as f1:
        nom_subdir = f1["nom"]
        keys = [
            key for key in nom_subdir.iteritems(cycle=False, filter_classname="TH1D")
        ]

        for key, hist in track(
            keys,
            description=f"Building nominal dataframe ({horn_polarity}, {run_id=})...",
        ):
            split = key.split(sep="_")
            match split:
                case [_, neutrino, category]:
                    pass
                case ["hnom", neutrino]:
                    category = "nominal"
                case ["hcv", neutrino]:
                    category = "central_value"
                case _:
                    continue

            df_list.append(
                construct_dataframe(
                    hist=hist,
                    horn_polarity=horn_polarity,
                    neutrino_mode=neutrino,
                    category=category,
                    run_id=run_id,
                )
            )

    nom_df = pd.concat(df_list, ignore_index=True)

    return nom_df


def construct_hadron_production_dataframe(
    rootfile: str,
    horn_polarity: str,
) -> pd.DataFrame:
    df_list: list[pd.DataFrame] = []

    with uproot.open(rootfile) as f:
        keys = [
            k
            for k in f.iteritems(
                cycle=False, filter_classname="TH1D", filter_name="/^.+?\\d$/"
            )
        ]

    for key, hist in track(
        keys, description=f"Building {horn_polarity} PPFX dataframe..."
    ):
        split = key.split(sep="/")
        neutrino_mode = split[0].split(sep="_")[0]
        universe = split[-1].split(sep="_")[-1]
        if len(split) == 3:
            ppfx_hp_syst = split[1]
        else:
            ppfx_hp_syst = split[0].split("_")[-1]

        df_list.append(
            construct_dataframe(
                hist=hist,
                horn_polarity=horn_polarity,
                neutrino_mode=neutrino_mode,
                category=ppfx_hp_syst,
                universe=int(universe),
            )
        )

    hp_df = pd.concat(df_list, ignore_index=True)

    return hp_df
