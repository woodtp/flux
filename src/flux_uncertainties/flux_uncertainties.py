import time
from argparse import ArgumentParser

from .analysis.flux_systematic_analysis import FluxSystematicsAnalysis
from .config import AnalysisConfig
from .data.exporter import Exporter
from .data.preprocessor import Preprocessor


def run():
    parser = ArgumentParser(
        prog="flux_uncertainties",
        description="Interpret PPFX output into a neutrino flux prediction with uncertainties",
    )
    parser.add_argument(
        "-c", "--config", help="specify the path to a yaml configuration file"
    )

    args = parser.parse_args()

    cfg_path = args.config

    cfg = AnalysisConfig.from_file(cfg_path)

    preprocessor = Preprocessor(
        results_directory=str(cfg.results_path),
        file_dict=cfg.file_dict,
        bin_edges=cfg.bin_edges,
    )

    analysis = FluxSystematicsAnalysis(
        nominal_flux_df=preprocessor.nominal_flux_df,
        ppfx_correction_df=preprocessor.ppfx_correction_df,
        bin_edges=preprocessor.bin_edges,
    )

    analysis.run(pca_threshold=1)
    ana_product_dict = analysis.exportable_product_dict

    with Exporter(results_directory=str(cfg.results_path)) as exporter:
        exporter.init_products_file()

        if ("fhc", 15) in preprocessor.file_dict:
            run15_fhc = preprocessor.file_dict[("fhc", 15)]
        else:
            run15_fhc = None
        if ("rhc", 15) in preprocessor.file_dict:
            run15_rhc = preprocessor.file_dict[("rhc", 15)]
        else:
            run15_rhc = None

        exporter.export_ppfx_output(fhc_file=run15_fhc, rhc_file=run15_rhc)

        exporter.export_products_dict(ana_product_dict)


if __name__ == "__main__":
    start = time.time()
    run()
    end = time.time()
    print(f"Finished in {end-start:0.2f} s")
